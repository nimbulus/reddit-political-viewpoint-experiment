# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 16:07:53 2018

"""
import utils
import numpy as np
import keras
from keras import optimizers
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, Flatten
from keras.layers import Embedding
from keras.models import Model
from keras.initializers import Constant
import tensorflow as tf
import matplotlib.pyplot as plt

#lets shuffle our data first
EXAMPLES_PATH = utils.randomize_record('examples-deduplicated.csv')
MAX_COMMENTS_SEQUENCE_LENGTH = utils.get_longest_record_length(EXAMPLES_PATH,'comments')
MAX_HKSUBS_SEQUENCE_LENGTH = utils.get_longest_record_length(EXAMPLES_PATH,'high_karma_subs')
MAX_LKSUBS_SEQUENCE_LENGTH = utils.get_longest_record_length(EXAMPLES_PATH,'low_karma_subs')
REDDITS_DIM = 3 #because we're assuming that these reddits will be just leftwing or rightwing, a line will satisfy the dimensionality of our examples.
EMBEDDING_DIM = 25 #for comments ~ #num_docs**0.25
FRACTION_TRAIN = .8 #how many we want for training



text_generator = utils.examples_generator(EXAMPLES_PATH,'comments')
labels_generator = utils.labels_generator(EXAMPLES_PATH,'label')
hksubs_generator = utils.examples_generator(EXAMPLES_PATH,'high_karma_subs')
lksubs_generator = utils.examples_generator(EXAMPLES_PATH,'low_karma_subs')
rw_heuristics_generator = utils.labels_generator(EXAMPLES_PATH,'rw_heuristics')#boilerplate for future heuristics.
lw_heuristics_generator = utils.labels_generator(EXAMPLES_PATH,'lw_heuristics')#boilerplate for future heuristics.


labels_list = list(labels_generator) #probably super memory intensive - find a workaround for this later
text_list = list(text_generator)
hksubs_list = list(hksubs_generator)
lksubs_list = list(lksubs_generator)
rw_heuristics_list = list(rw_heuristics_generator)
lw_heuristics_list = list(lw_heuristics_generator)

train_size = int(len(text_list)*FRACTION_TRAIN)

training_text = text_list[:train_size]
training_hksubs = hksubs_list[:train_size]
training_lksubs = lksubs_list[:train_size]
training_labels = labels_list[:train_size]
rw_training_heuristics = rw_heuristics_list[:train_size]
lw_training_heuristics = lw_heuristics_list[:train_size]

test_text = text_list[train_size:]
test_hksubs = hksubs_list[train_size:]
test_lksubs = lksubs_list[train_size:]
test_labels = labels_list[train_size:]
test_rw_heuristics = rw_heuristics_list[train_size:]
test_lw_heuristics = lw_heuristics_list[train_size:]

comment_tokenizer = Tokenizer()
comment_tokenizer.fit_on_texts(text_list) # built-in embedding fitter
comments_sequences = comment_tokenizer.texts_to_sequences(training_text) #converts text from text_generator into list of vectors
comments_word_index = comment_tokenizer.word_index # a dict of word to indexed dict

test_comments_sequences = comment_tokenizer.texts_to_sequences(test_text)

#high_karma_subs embeddings
reddits_tokenizer = Tokenizer()


reddits_tokenizer.fit_on_texts(hksubs_list+lksubs_list) #fit accross the entire space of sub examples. 
hksubs_sequences = reddits_tokenizer.texts_to_sequences(training_hksubs)
lksubs_sequences = reddits_tokenizer.texts_to_sequences(training_lksubs)
reddits_word_index = reddits_tokenizer.word_index

test_hksubs_sequences = reddits_tokenizer.texts_to_sequences(test_hksubs)
test_lksubs_sequences = reddits_tokenizer.texts_to_sequences(test_lksubs)


embeddings_index = {} 
with open('glove.twitter.27B.25d.txt', 'r', encoding='UTF-8') as f: 
    #load the glove pretrained model, insert vectors into place to create a (massive) matrix.
    #goddamn windows with their bullshit cp1252 wierdness. 
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
glove_embedding_matrix = np.zeros((len(comments_word_index), EMBEDDING_DIM))
for word, i in comments_word_index.items(): #word index is a 'word':indexed_num
    if i > len(comments_word_index): #we have more words indexed than we have in the document??
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        glove_embedding_matrix[i-1] = embedding_vector




comments_embedding_layer = Embedding(len(comments_word_index)+1, #there is an off-by-one error, probably because the same wrapper is used for R which starts its index from 1.
                            EMBEDDING_DIM, #output dimensionality
                            embeddings_initializer=Constant(glove_embedding_matrix), #initiates the matrix with the values from the extracted glove data
                            input_length=MAX_COMMENTS_SEQUENCE_LENGTH,
                            trainable=False) #pre-trained

reddits_embedding_layer = Embedding(len(reddits_word_index)+1,
                            REDDITS_DIM,
                            #embeddings_initializer=Constant(embedding_matrix),
                            input_length=max(MAX_HKSUBS_SEQUENCE_LENGTH, MAX_LKSUBS_SEQUENCE_LENGTH), #just has to fit their entirety.
                            trainable=True) #not pretrained, must be trained by the model

#building layers!
comments_sequence_input = Input(shape=(MAX_COMMENTS_SEQUENCE_LENGTH,), dtype='int32', name="comments_input")
hksubs_sequence_input = Input(shape=(MAX_HKSUBS_SEQUENCE_LENGTH,), dtype='int32', name="hksubs_input")
lksubs_sequence_input = Input(shape=(MAX_LKSUBS_SEQUENCE_LENGTH,), dtype='int32', name="lksubs_input")
rw_heuristics_input = Input(shape=(1,),dtype='float32',name = "rw_heuristics_input")
lw_heuristics_input = Input(shape=(1,),dtype='float32',name = "lw_heuristics_input")

embedded_comments = comments_embedding_layer(comments_sequence_input)
embedded_hksubs = reddits_embedding_layer(hksubs_sequence_input)
embedded_lksubs = reddits_embedding_layer(lksubs_sequence_input)

flattened_hksubs = Flatten(name="flattened_hksubs")(embedded_hksubs)
flattened_lksubs = Flatten(name="flattened_lksubs")(embedded_lksubs)
flattened_comments = Flatten(name="flatened_comments")(embedded_comments)
merged_embeddings = keras.layers.Concatenate(axis=-1,name="merged_embeddings")([flattened_hksubs,flattened_lksubs, flattened_comments]) #very memory intensive - try using a smaller GLoVE embedding
layer1 = Dense(10, activation='relu',name="layer1",activity_regularizer=keras.regularizers.l1(0.01))(merged_embeddings)
merged_heuristics = merged_embeddings = keras.layers.Concatenate(axis=-1,name="merged_heuristics")([layer1,rw_heuristics_input,lw_heuristics_input])
layer2 = Dense(10, activation='relu',name="layer2",activity_regularizer=keras.regularizers.l2(0.01))(merged_heuristics)
layer3 = Dense(6, activation='relu',name="layer3",activity_regularizer=keras.regularizers.l2(0.02))(layer2)
preds = Dense(2, activation='softmax',name="output")(layer3) 


nadam = keras.optimizers.Nadam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)

auc_roc = utils.as_keras_metric(tf.metrics.auc)
model = Model(inputs=[comments_sequence_input,hksubs_sequence_input,lksubs_sequence_input,rw_heuristics_input,lw_heuristics_input], outputs=preds)
model.compile(loss='categorical_crossentropy',
              optimizer=nadam,
              metrics=['acc',auc_roc])

comments_data = pad_sequences(comments_sequences, maxlen=MAX_COMMENTS_SEQUENCE_LENGTH) #ndarray of NUMBEROFSEQUENCES x MAXLENGTH
hksubs_data = pad_sequences(hksubs_sequences, maxlen=MAX_HKSUBS_SEQUENCE_LENGTH)
lksubs_data = pad_sequences(lksubs_sequences, maxlen=MAX_LKSUBS_SEQUENCE_LENGTH)
###
test_comments_data = pad_sequences(test_comments_sequences, maxlen=MAX_COMMENTS_SEQUENCE_LENGTH)
test_hksubs_data = pad_sequences(test_hksubs_sequences, maxlen=MAX_HKSUBS_SEQUENCE_LENGTH)
test_lksubs_data = pad_sequences(test_lksubs_sequences, maxlen=MAX_LKSUBS_SEQUENCE_LENGTH)

training_generator = utils.dataGenerator(comments_data,MAX_COMMENTS_SEQUENCE_LENGTH,
                                         hksubs_data,MAX_HKSUBS_SEQUENCE_LENGTH,
                                         lksubs_data,MAX_LKSUBS_SEQUENCE_LENGTH,
                                         rw_training_heuristics,lw_training_heuristics,
                                         training_labels,32)#smaller batch sizes to save memory for each round?

test_generator = utils.dataGenerator(test_comments_data,MAX_COMMENTS_SEQUENCE_LENGTH,
                                     test_hksubs_data,MAX_HKSUBS_SEQUENCE_LENGTH,
                                     test_lksubs_data,MAX_LKSUBS_SEQUENCE_LENGTH,
                                     test_rw_heuristics,test_lw_heuristics,
                                     test_labels,16)
history = model.fit_generator(training_generator,
          epochs=25,
          verbose=1,
          validation_data=test_generator,
          validation_steps=20,
          steps_per_epoch=20) #steps per epoch = number of unique examples divided by batch size. 
         
#plot metrics
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.plot(history.history['auc'])
plt.plot(history.history['val_auc'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
