#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This module collects redditors by username from a given subreddit, and can filter given a minimum threshold 
karma. You should then go and tag these users yourself manually. If you'd like, you can autotag them 0, for 
for rightwing, or 1 for leftwing, by the subreddit the user posted in. 


invoke it by $python3 ./subreddit_user_collector SUBREDDIT MAX=200 MINKARMA=0
"""

import praw
import argparse
import json
import csv
from more_itertools import unique_everseen

parser = argparse.ArgumentParser(description='Automatically collect users from subreddit, and exports to redditors.csv. You will need to manually tag if you do not specify -t')
parser.add_argument('-s', '--subreddit',required=True, metavar='subreddit name', type=str, nargs=1, help='subreddit to cull users from', dest='subreddit')
parser.add_argument('-m', '--max', metavar='max_users', type=int, nargs='?', help='maximum users to cull', dest='max_users', default=50)
parser.add_argument('-k', '--karma', metavar='min_karma', type=int, nargs='?', help='minimum karma in subreddit', dest='min_karma', default=0)
parser.add_argument('-t', '--tag', metavar='tag_user', type=int, nargs='?', help='Automatically tag user as 0 (rw) or 1 (lw)')
parser.add_argument('-o', '--output', metavar='output_file', type=str, nargs='?', help='output file to write to',dest='output_file', default="redditors.csv")

args = parser.parse_args() #subreddits, max_users, min_karma, tag

blacklisted_users = ['automoderator','AAbot','Antiracism_Bot','xkcd_bot	'] #blacklist bots


with open('auth.json') as f:
    authSettings = f.read()
    authSettings = json.loads(authSettings)

reddit = praw.Reddit(client_id=authSettings['client_id'], \
                     client_secret=authSettings['client_secret'], \
                     user_agent='scrapper')

def main():
    subreddit = reddit.subreddit(args.subreddit[0])
    iterable = 0
    for post in subreddit.hot(limit=args.max_users):
        print(iterable)
        author = post.author
        try:
            if author.name in blacklisted_users:
                pass
            if post.score < args.min_karma:
                pass
            with open(args.output_file, mode='a') as redditors:
                redditors_writer = csv.writer(redditors, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                redditors_writer.writerow([author.name, args.tag])
            iterable = iterable+1
            if iterable == args.max_users:
                return 0
        except AttributeError:
            #we've reached the end of the generator, and author doesn't exist
            return 0
    
main()
with open(args.output_file,'r') as f, open(args.output_file+'-deduplicated.csv','w') as out_file:
    out_file.writelines(unique_everseen(f))