# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 17:54:10 2018

@author: us72271
"""
import csv
csv.field_size_limit(1000000000)
import ast
import pandas as pd
import numpy as np
import random
from keras.utils import to_categorical
import praw
import json
from collections import Counter
import reddits
import matplotlib.pyplot as plt


def generate_heuristics_for_redditor(redditor):
    with open('auth.json') as f:
        authSettings = f.read()
    authSettings = json.loads(authSettings)
    reddit = praw.Reddit(client_id=authSettings['client_id'], \
                     client_secret=authSettings['client_secret'], \
                     user_agent='scrapper')
    redditor = reddit.redditor(redditor)
    subsPosted = {}
    karma_of_interest=0
    right_wing_reddits = reddits.right_wing_reddits
    left_wing_reddits = reddits.left_wing_reddits
    for comment in redditor.comments.new(limit=None):
        if comment.subreddit.display_name in subsPosted: #do we have this sub already?
            subsPosted.update({comment.subreddit.display_name:subsPosted[comment.subreddit.display_name]+comment.score})
        else: #already exists
            subsPosted.update({comment.subreddit.display_name:comment.score})
    subsPosted = Counter(subsPosted)
    for subreddit, karma in subsPosted.most_common(): #rw heuristics
        if subreddit.lower() in right_wing_reddits: 
            karma_of_interest = karma_of_interest+karma
    rw_heuristics = float(karma_of_interest)/float(sum(subsPosted.values()))
    karma_of_interest = 0
    for subreddit, karma in subsPosted.most_common(): #lw heuristics
        if subreddit.lower() in left_wing_reddits: 
            karma_of_interest = karma_of_interest+karma
    lw_heuristics = float(karma_of_interest)/float(sum(subsPosted.values()))
    return np.array([[rw_heuristics,lw_heuristics]])

def randomize_record(csvfile):
    
    """
    expects:
        path to csvfile
    returns:
        path to shuffled csvfile
    """
    df = pd.read_csv(csvfile, index_col=False)
    writeout = df.sample(frac=1.0)
    writeout.to_csv(csvfile+'-shuffled.csv', index=False)
    return csvfile+'-shuffled.csv'
    
def get_longest_record_length(record,field):
    """expects:
        record = path_to_csv
        field = relevant field you desire the longest record of
    
    returns:
        int representing the length of the longest record in the relevant field
        """
    length = 0
    with open(record, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        count = 0
        for row in reader:
            print(count)
            count = count+1
            if len(ast.literal_eval(row[field])) > length:
                length = len(ast.literal_eval(row[field]))
    return length

def examples_generator(record,field):
    #todo: Randomize and shuffle
    """expects:
        record = path to csv
        field = relevant field
    
    returns:
        joined string from field without loading entire csv into memory
        """
    fieldstring = " "
    with open(record, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            fieldstring = " ".join(ast.literal_eval(row[field]))
            yield fieldstring
def labels_generator(record,field):
    #todo: Randomize and shuffle
    """expects:
        record = path to csv
        field = relevant field
    
    returns:
        joined string from field without loading entire csv into memory
        """
    with open(record, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            try:
                fieldstring = float(row[field])
            except ValueError:
                print(row[field])
                break
            yield fieldstring

def as_keras_metric(method):
    import functools
    from keras import backend as K
    import tensorflow as tf
    @functools.wraps(method)
    def wrapper(self, args, **kwargs):
        """ Wrapper for turning tensorflow metrics into keras metrics """
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper

def dataGenerator(rw_heuristics,lw_heuristics, labels, batch_size):
    #assume comments,reddits is an array for their data as comments_data is
    # Create empty arrays to contain batch of features and labels#
    
#    batch_comments = np.zeros((batch_size, COMMENTS_SIZE)) #magic number is size of comments_data 
#    batch_hksubs = np.zeros((batch_size,HKSUBS_SIZE))#size of reddits_data
#    batch_lksubs = np.zeros((batch_size,LKSUBS_SIZE))
    batch_rw_heuristics = np.zeros((batch_size,1))
    batch_lw_heuristics = np.zeros((batch_size,1))
    batch_labels_unprocessed = np.zeros((batch_size,1)) # labels are each [0,1]#conservative or [1,0]#liberal
    
    while True:
        for i in range(batch_size):
            index = random.randint(0,(len(rw_heuristics)-1)) #len comments == len reddits = number of examples
            #print(comments.shape)
#            batch_comments[i] = comments[index]
#            batch_hksubs[i] = hksubs[index]
#            batch_lksubs[i] = lksubs[index]
            batch_labels_unprocessed[i] = labels[index]
            batch_rw_heuristics[i] = rw_heuristics[index]
            batch_lw_heuristics[i] = lw_heuristics[index]
        batch_labels = to_categorical(batch_labels_unprocessed,num_classes=2)
        yield ({'rw_heuristics_input':batch_rw_heuristics,'lw_heuristics_input':batch_lw_heuristics}, {'output': batch_labels})
def visualize_model(mdl,fidelity):
    xrange = np.linspace(-2.0,2.0, num=fidelity)
    yrange = xrange.copy()
    z = []
    for i in xrange:
        for j in yrange:
            z.append(int(mdl.predict(np.array([[i,j]])).argmax(-1)))
    z = np.array(z)
    z = z.reshape(fidelity,fidelity)
    plt.imshow(z)
    plt.gca().invert_yaxis()
