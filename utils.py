# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 17:54:10 2018

@author: us72271
"""
import csv
csv.field_size_limit(100000000)
import ast
import pandas as pd
import numpy as np
import random
from keras.utils import to_categorical


def randomize_record(csvfile):
    """
    expects:
        path to csvfile
    returns:
        path to shuffled csvfile
    """
    df = pd.read_csv(csvfile, index_col=False)
    writeout = df.sample(frac=1.0)
    writeout.to_csv(csvfile+'-shuffled.csv', index=False)
    return csvfile+'-shuffled.csv'
    
def get_longest_record_length(record,field):
    """expects:
        record = path_to_csv
        field = relevant field you desire the longest record of
    
    returns:
        int representing the length of the longest record in the relevant field
        """
    length = 0
    with open(record, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if len(ast.literal_eval(row[field])) > length:
                length = len(ast.literal_eval(row[field]))
    return length

def examples_generator(record,field):
    #todo: Randomize and shuffle
    """expects:
        record = path to csv
        field = relevant field
    
    returns:
        joined string from field without loading entire csv into memory
        """
    fieldstring = " "
    with open(record, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            fieldstring = " ".join(ast.literal_eval(row[field]))
            yield fieldstring
def labels_generator(record,field):
    #todo: Randomize and shuffle
    """expects:
        record = path to csv
        field = relevant field
    
    returns:
        joined string from field without loading entire csv into memory
        """
    with open(record, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            fieldstring = ast.literal_eval(row[field])
            yield fieldstring

def as_keras_metric(method):
    import functools
    from keras import backend as K
    import tensorflow as tf
    @functools.wraps(method)
    def wrapper(self, args, **kwargs):
        """ Wrapper for turning tensorflow metrics into keras metrics """
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper

def dataGenerator(comments,COMMENTS_SIZE,hksubs,HKSUBS_SIZE,lksubs,LKSUBS_SIZE, rw_heuristics,lw_heuristics, labels, batch_size):
    #assume comments,reddits is an array for their data as comments_data is
    # Create empty arrays to contain batch of features and labels#
    
    batch_comments = np.zeros((batch_size, COMMENTS_SIZE)) #magic number is size of comments_data 
    batch_hksubs = np.zeros((batch_size,HKSUBS_SIZE))#size of reddits_data
    batch_lksubs = np.zeros((batch_size,LKSUBS_SIZE))
    batch_rw_heuristics = np.zeros((batch_size,1))
    batch_lw_heuristics = np.zeros((batch_size,1))
    batch_labels_unprocessed = np.zeros((batch_size,1)) # labels are each [0,1]#conservative or [1,0]#liberal
    
    while True:
        for i in range(batch_size):
            index = random.randint(0,(len(comments)-1)) #len comments == len reddits = number of examples
            #print(comments.shape)
            batch_comments[i] = comments[index]
            batch_hksubs[i] = hksubs[index]
            batch_lksubs[i] = lksubs[index]
            batch_labels_unprocessed[i] = labels[index]
            batch_rw_heuristics[i] = rw_heuristics[index]
            batch_lw_heuristics[i] = lw_heuristics[index]
        batch_labels = to_categorical(batch_labels_unprocessed)
        yield ({'comments_input': batch_comments, 'hksubs_input': batch_hksubs,'lksubs_input': batch_lksubs, 'rw_heuristics_input':batch_rw_heuristics,'lw_heuristics_input':batch_lw_heuristics}, {'output': batch_labels})