#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 21:12:37 2018

@praw collector for data scrapes


Collects redditor's comments and subreddits posted in. 

TODO: Change the records to add new field "highest_karma_subs" "lowest_karma_subs"

"""

import praw
import re
import string
#import tensorflow as tf
import json
import csv
from collections import Counter
from math import floor
from urllib.error import HTTPError
from more_itertools import unique_everseen
from tqdm import tqdm
import reddits
with open('auth.json') as f:
    authSettings = f.read()

authSettings = json.loads(authSettings)

#train_filename = 'train.tfrecord'
#writer = tf.python_io.TFRecordWriter(train_filename)

left_wing_reddits = reddits.left_wing_reddits

right_wing_reddits =reddits.right_wing_reddits

punctuationtable = str.maketrans(dict.fromkeys(string.punctuation))


reddit = praw.Reddit(client_id=authSettings['client_id'], \
                     client_secret=authSettings['client_secret'], \
                     user_agent='scrapper')


def usersGenerator(csvFile):
    #yields a user from a file
    with open(csvFile, mode='r') as redditors:
        redditors = csv.reader(redditors, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in redditors:
            yield row # ['username','tag type==int']


def main_iterator(users):
    for user in users:
        redditor = reddit.redditor(user[0])
        label = user[1] # 0=rightwing, 1=leftwing
        commitDict = {'high_karma_subs':[], 'low_karma_subs':[],
                      'comments':[],'rw_heuristics':0.0,'lw_heuristics':0.0,'label':float(label),
                      }
        subsPosted = {} # {'the_donald':#total_karma}
        karma_of_interest = 0 #total karma from subs in rightwing or leftwing.
        try:
            for comment in redditor.comments.new(limit=None): # grab comments and their respective subreddits and karmas
                commentText = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', comment.body) #strip urls
                commentText = re.sub(r'[^\x00-\x7F]+','', commentText) #strip non-ascii
                commentText = commentText.lower()
                commentText = commentText.split(' ')
                commitDict['comments'] = commitDict['comments']+commentText
                if comment.subreddit.display_name in subsPosted: #do we have this sub already?
                    subsPosted.update({comment.subreddit.display_name:subsPosted[comment.subreddit.display_name]+comment.score})
                else: #already exists
                    subsPosted.update({comment.subreddit.display_name:comment.score})
            subsPosted = Counter(subsPosted)
            
            for subreddit, karma in subsPosted.most_common(floor(len(subsPosted)*0.25)): # get top 25%
                commitDict['high_karma_subs'].append(subreddit)
            for subreddit, karma in subsPosted.most_common()[-floor(len(subsPosted)*.25): ]: # get lowest 25%
                commitDict['low_karma_subs'].append(subreddit) 
            
            for subreddit, karma in subsPosted.most_common(): #rw heuristics
                if subreddit.lower() in right_wing_reddits: 
                    karma_of_interest = karma_of_interest+karma
            commitDict['rw_heuristics'] = float(karma_of_interest)/float(sum(subsPosted.values()))
            karma_of_interest = 0
            for subreddit, karma in subsPosted.most_common(): #lw heuristics
                if subreddit.lower() in left_wing_reddits: 
                    karma_of_interest = karma_of_interest+karma
            commitDict['lw_heuristics'] = float(karma_of_interest)/float(sum(subsPosted.values()))
            #print(commitDict)
            #headers = ['high_karma_subs','low_karma_subs','comments','rw_heuristics','lw_heuristics','label']
            with open('examples.csv', 'a') as f:  
                w = csv.DictWriter(f, commitDict.keys(),lineterminator='\n',)#fieldnames=#headers)
                #w.writeheader() #header should already exist. if not, write high_karma_subs,low_karma_subs,comments,label to the front
                w.writerow(commitDict)
        except:
            print("error 403")
            pass
            
tqdm(main_iterator(usersGenerator('redditors-deduplicated.csv')),total=1381)

with open('examples.csv','r') as f, open('examples-deduplicated.csv','a') as out_file:
    out_file.writelines(unique_everseen(f))