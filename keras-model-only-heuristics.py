# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 12:59:39 2018

@author: US72271
"""

import utils_only_heuristics
import numpy as np
from numpy import array
import keras
from keras import optimizers
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, Flatten, BatchNormalization
from keras.layers import Embedding
from keras.models import Model
from keras.utils import to_categorical
from keras.initializers import Constant
import tensorflow as tf
import matplotlib.pyplot as plt
from time import sleep


def results(prediction):
    return prediction.argmax(axis=-1)

EXAMPLES_PATH = utils_only_heuristics.randomize_record('examples-deduplicated.csv')

rw_heuristics_generator = utils_only_heuristics.labels_generator(EXAMPLES_PATH,'rw_heuristics')#boilerplate for future heuristics.
lw_heuristics_generator = utils_only_heuristics.labels_generator(EXAMPLES_PATH,'lw_heuristics')#boilerplate for future heuristics.
labels_generator = utils_only_heuristics.labels_generator(EXAMPLES_PATH,'label')
y = array(list(labels_generator)) #probably super memory intensive - find a workaround for this later

rw_heuristics_list = list(rw_heuristics_generator)
lw_heuristics_list = list(lw_heuristics_generator)

x = array(list(zip(rw_heuristics_list,lw_heuristics_list))) #array([[0.00,1.00],[1.32,0.23]... ])

y_cat = to_categorical(y,num_classes=2)
x_input = Input(shape=(2,),dtype='float32',name = "x_input")
normalize = BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(x_input)
layer1 = Dense(32, activation='relu',name="layer1",)(x_input)
layer2 = Dense(32, activation='relu',name="layer2",)(layer1)
layer3 = Dense(12, activation='relu',name="layer3",)(layer2)
preds = Dense(2,activation='sigmoid',name="output")(layer3) 

auc_roc = utils_only_heuristics.as_keras_metric(tf.metrics.auc)
nadam = keras.optimizers.Nadam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)
model = Model(inputs=x_input, outputs=preds)
model.compile(loss='binary_crossentropy',
              optimizer=nadam,
              metrics=['acc',auc_roc])
def pretty_train(breaks):
    for i in range(breaks):
        history = model.fit(
            x,
            y_cat,
            validation_split=0.3,
            epochs=100,
            batch_size=10,
            verbose=0
            )
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        
        plt.plot(history.history['auc'])
        plt.plot(history.history['val_auc'])
        plt.title('AUC_ROC over time')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        utils_only_heuristics.visualize_model(model,100)
        sleep(5) #take a break so we can see it
    return model

pretty_train(5)

