# rightwing-leftwing
Guess a redditor's political affiliation various features

This is still very early stage work. Currently, users.py and prawcollector.py have been largely completed, 
and can be used standalone to collect users and generate training data respectively.

Heuristics-only model can now reach an AUC of ~0.96 without excess overfitting. 

# Future work

* adding more dimensionality to where redditors are placed, instead of a 1d left-right spectrum  (2d political compass?)
* web-based interface to stalk redditors with - similar fashion to snoopsnoo.

# Caveats

This only works for anglophone redditors. 

# LICENCE
GNU GPL v3